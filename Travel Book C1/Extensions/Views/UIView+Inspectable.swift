//
//  UIView+Inspectable.swift
//  Travel Book C1
//
//  Created by Christian Iñigo De Leon Alvarez on 23/10/2017.
//  Copyright © 2017 Travel Book Philippines Inc. All rights reserved.
//

import UIKit

@IBDesignable
extension UIView {
    @IBInspectable
    var borderRadius : CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set (newValue) {
            self.layer.cornerRadius = newValue
        }
    }
}
