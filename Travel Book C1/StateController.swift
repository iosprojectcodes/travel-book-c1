//
//  StateController.swift
//  Travel Book C1
//
//  Created by Christian Iñigo De Leon Alvarez on 23/10/2017.
//  Copyright © 2017 Travel Book Philippines Inc. All rights reserved.
//

import Foundation
import Alamofire

class StateController {
    private (set) var hotels = [Hotel]()
    var destination = ""
    
    //Search options
    struct basicURLOptions {
        var activeSort = 0
        var adultNumMax = 2
        var adultNumMin = 2
        var afCd = 13
        var apiKey = "M0b1l3n2Q7k7"
        var cityCd = "PH4800"
        var currentPage = 1
        var night = 1
        var room = 1
        var stayDay = "2017-10-26"
        
        func string() -> String {
            return "?activeSort=\(activeSort)&adultNumMax=\(adultNumMax)&adultNumMin=\(adultNumMin)&afCd=\(afCd)&apiKey=\(apiKey)&cityCd=\(cityCd)&currentPage=\(currentPage)&night=\(night)&room=\(room)&stayDay=\(stayDay)"
        }
        
    }
    
    //MARK: - Fetch Hotels from API
    func performRequest(completionHandler: @escaping ()->Void){
        let baseURLString = "http://jws.dv01.travelbook.ph/HotelSearchAPI/v1/hotel/app/list"
        var authURLString = ""
        if destination == "" {
            authURLString = "?activeSort=0&adultNumMax=2&adultNumMin=2&afCd=13&apiKey=M0b1l3n2Q7k7&cityCd=PH4800&currentPage=1&night=1&room=1&stayDay=2017-10-26"
        } else {
            var authURL = basicURLOptions()
            switch destination {
            case "Cebu":
                authURL.cityCd = "PH2500"
            case "Palawan":
                authURL.cityCd = "PH5800"
            case "Baguio":
                authURL.cityCd = "PH1200"
            case "Boracay":
                authURL.cityCd = "PH0300"
            case "Bohol":
                authURL.cityCd = "PH1500"
            default:
                authURL.cityCd = "PH4800"
            }
            
            authURLString = authURL.string()
            print("String is: \(authURLString)")
        }
    
        let headers = ["X-Auth-UserId": "ios141215",
                       "X-Auth-Token": "47D858616714367B",
                       "Authorization": "Basic amVlcG5leTo3UGhwMkdvIQ==",
                       "X-Authorization": "f924a85d8a3fbfeb1b23c97dbf8c1021e1ad0889"]
        
        Alamofire.request(baseURLString+authURLString, headers: headers).validate().responseJSON { (response) in
            // print("Total results: \(response)")
            
            if let status = response.response?.statusCode {
                switch status {
                case 200...400:
                    print("Success!")
                default:
                    print("Error")
                }
            }
            
            if let result = response.result.value as? [String:Any] {
                print("Total hotels: \(result["totalResult"] ?? "no results")")
                self.hotels = self.getHotelsList(from: result)
                completionHandler()
            } else {
                print("Error serializing JSON string.")
            }
            
        }
    }
    
    func resetHotelsList() {
        hotels = [Hotel]()
        
    }
    
    //Takes the data from the dictionary and adds it to the model array.
    private func getHotelsList(from jsonDictionary: [String: Any] ) -> [Hotel] {
        
        if let hotelsDictionary = jsonDictionary["hotelData"] as? [[String: Any]] {
            for hotelData in hotelsDictionary {
                var newHotel = Hotel()
                newHotel.name = hotelData["name"] as? String ?? ""
                //print("Hotel data: \(hotelData)")
                hotels.append(newHotel)
            }
        } else {
            print("No hotel data found.")
        }
        
        return hotels
    }
    
    //MARK: - Popular Destinations
    
    enum PopularDestination {
        case cebu(String)
    }
    
    func getPopularDestinations() -> [String]? {
        
        //        if let fileURL = Bundle.main.url(forResource: "popularDestination", withExtension: "plist") {
        //            do {
        //                let data = try Data(contentsOf: fileURL)
        //                do {
        //                   try PropertyListSerialization.data(fromPropertyList: data, format: .xml, options: nil )
        //                } catch let error as NSError {
        //                    print("Error serializing popular destinations! \(error)")
        //                }
        //            } catch let error as NSError {
        //                print("Error obtaining Popular Destinations! \(error)")
        //            }
        //        }
        //
        
        //Temporary static dictionary
        
        //        let popularDestinations = [["name": "Cebu",
        //                                    "id":"PH2500"],
        //                                   ["name": "Manila",
        //                                    "id":"PH4800"],
        //                                   ["name": "Palawan",
        //                                    "id":"PH5800"],
        //                                   ["name": "Baguio",
        //                                    "id":"PH1200"],
        //                                   ["name": "Boracay",
        //                                    "id":"PH0300"],
        //                                   ["name": "Bohol",
        //                                    "id":"PH1500"]]
        //
        let popularDestinations = ["Cebu","Manila","Palawan","Baguio","Boracay","Bohol"]
        
        
        
        return popularDestinations
    }
    
    
    
    
    
    
    
    
    
    
    
    //MARK: - Helper methods
    
    func getString(from date: Date) -> String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd")
        return formatter.string(from: date)
    }
    
    
    
}
