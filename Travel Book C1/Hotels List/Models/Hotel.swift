//
//  Hotel.swift
//  Travel Book C1
//
//  Created by Christian Iñigo De Leon Alvarez on 23/10/2017.
//  Copyright © 2017 Travel Book Philippines Inc. All rights reserved.
//

import Foundation

struct Hotel {
    var name = ""
    var location = ""
    struct rating {
        var starCount: Int = 0
        var reviewCount: Float = 0
    }
    
    var canPayAtHotel = false
    var imageURL: URL?
    var roomsLeft = 0
    struct price {
        var original: Double = 0
        var discounted: Double = 0
        var discount: String {
            let discountPercentage = Int((original-discounted)/original) * 100
            return String(discountPercentage)
        }
    }
    var pointsEarned = 0
    
}
