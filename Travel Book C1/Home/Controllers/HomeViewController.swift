//
//  FirstViewController.swift
//  Travel Book C1
//
//  Created by Christian Iñigo De Leon Alvarez on 23/10/2017.
//  Copyright © 2017 Travel Book Philippines Inc. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var stateController: StateController?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK: - Popular Destinations
    
    @IBOutlet weak var popularDestinationsCollectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let popularDestinationsCount = stateController?.getPopularDestinations()?.count {
            return popularDestinationsCount
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DestinationCell", for: indexPath) as? PopularDestinationCell {
            cell.setDestination(text: (stateController?.getPopularDestinations()![indexPath.row])!)
            
            return cell
        }
        
       // popularDestinationText.text = stateController?.getPopularDestinations()![indexPath.row]
        
        return UICollectionViewCell()
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PopularDestinationSegue",
            let destinationVC = segue.destination as? HotelsListTableViewController{
            self.stateController?.destination = (self.stateController?.getPopularDestinations()![(popularDestinationsCollectionView.indexPathsForSelectedItems?.first?.row)!])!
            destinationVC.stateController = self.stateController
            
        }
    }
    
    
}

