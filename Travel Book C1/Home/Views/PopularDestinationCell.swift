//
//  PopularDestinationCell.swift
//  Travel Book C1
//
//  Created by Christian Iñigo De Leon Alvarez on 23/10/2017.
//  Copyright © 2017 Travel Book Philippines Inc. All rights reserved.
//

import UIKit

class PopularDestinationCell: UICollectionViewCell {
    @IBOutlet weak var destinationTextLabel: UILabel!
    
    func setDestination(text: String) {
        destinationTextLabel.text = text

        let baseMaximumTextCount = 5
        if text.count > baseMaximumTextCount {
            destinationTextLabel.bounds = CGRect(origin: destinationTextLabel.bounds.origin, size: CGSize(width: destinationTextLabel.bounds.width + CGFloat(text.count - baseMaximumTextCount)*10, height: destinationTextLabel.bounds.height))
        }

    }
    
}
